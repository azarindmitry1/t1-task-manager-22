package ru.t1.azarin.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    public final static String NAME = "about";

    public final static String ARGUMENT = "-a";

    public final static String DESCRIPTION = "Display developer info.";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Dmitry Azarin");
        System.out.println("azarindmitry1@gmail.com");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
