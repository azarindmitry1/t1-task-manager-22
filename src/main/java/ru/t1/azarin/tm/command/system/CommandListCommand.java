package ru.t1.azarin.tm.command.system;

import ru.t1.azarin.tm.api.model.ICommand;
import ru.t1.azarin.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandListCommand extends AbstractSystemCommand {

    public final static String NAME = "commands";

    public final static String ARGUMENT = "-cmd";

    public final static String DESCRIPTION = "Display commands of application.";

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getTerminalCommands();
        for (final ICommand command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
