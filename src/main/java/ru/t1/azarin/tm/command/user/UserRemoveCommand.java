package ru.t1.azarin.tm.command.user;

import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractUserCommand {

    public final static String NAME = "user-remove";

    public final static String DESCRIPTION = "Remove user.";

    @Override
    public void execute() {
        System.out.println("[USER REMOVE]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeByLogin(login);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
