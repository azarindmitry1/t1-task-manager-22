package ru.t1.azarin.tm.command.task;

import ru.t1.azarin.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    public final static String NAME = "task-remove-by-id";

    public final static String DESCRIPTION = "Remove task by id.";

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        serviceLocator.getTaskService().removeById(userId, id);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
