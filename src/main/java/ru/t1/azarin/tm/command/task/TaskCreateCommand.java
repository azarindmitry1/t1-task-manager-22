package ru.t1.azarin.tm.command.task;

import ru.t1.azarin.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    public final static String NAME = "task-create";

    public final static String DESCRIPTION = "Create new task.";

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final String userId = getUserId();
        serviceLocator.getTaskService().create(userId, name, description);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
