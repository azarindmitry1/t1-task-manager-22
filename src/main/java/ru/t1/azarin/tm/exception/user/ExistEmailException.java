package ru.t1.azarin.tm.exception.user;

public class ExistEmailException extends AbstractUserException {

    public ExistEmailException() {
        super("Error! Email is exist...");
    }

}
