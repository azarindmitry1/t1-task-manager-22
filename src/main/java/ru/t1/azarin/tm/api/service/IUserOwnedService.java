package ru.t1.azarin.tm.api.service;

import ru.t1.azarin.tm.api.repository.IUserOwnedRepository;
import ru.t1.azarin.tm.enumerated.Sort;
import ru.t1.azarin.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M> {

    List<M> findAll(String userId, Sort sort);

}
